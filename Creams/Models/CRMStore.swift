//
//  CRMStore.swift
//  Creams
//
//  Created by Sulley on 13/05/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit

class CRMStore: NSObject {
    let storeName : String
    let address : String
    let town : String
    let postcode : String
    let phoneNumber : String
    let emailAddress : String
    
    init(storeName: String, address: String, town: String, postcode: String, phoneNumber: String, emailAddress: String) {
        self.storeName = storeName
        self.address = address
        self.town = town
        self.postcode = postcode
        self.phoneNumber = phoneNumber
        self.emailAddress = emailAddress
        
        super.init()
    }
}
