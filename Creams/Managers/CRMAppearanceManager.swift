//
//  CRMAppearanceManager.swift
//  Creams
//
//  Created by Sulley on 13/05/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit
import ChameleonFramework

class CRMAppearanceManager: NSObject {
    
    static let sharedInstance = CRMAppearanceManager()
    
    // Font Sizes
    let kRegularFontSize : CGFloat = 20.0
    let kNavBarFontSize : CGFloat = 22.0
    let kNavBarButtonFontSize : CGFloat = 20.0
    
    // Fonts
    let kRegularFontName = "Oswald-Regular"
    let kBoldFontName = "Oswald-Regular"
    let kNavBarFontName = "Oswald-Regular"

    var defaultFont : UIFont?
    var defaultBoldFont : UIFont?
    var defaultNavBarFont : UIFont?
    var defaultNavButtonBarFont : UIFont?

    override init() {
        super.init()
        
        println("Appearance Manager initialised...")
        
        self.defaultFont = UIFont(name: kRegularFontName, size: kRegularFontSize)
        self.defaultBoldFont = UIFont(name: kBoldFontName, size: kRegularFontSize)
        self.defaultNavBarFont = UIFont(name: kNavBarFontName, size: kNavBarFontSize)
        self.defaultNavButtonBarFont = UIFont(name: kNavBarFontName, size: kNavBarButtonFontSize)
    }
    
    func defaultFontWithSize(size: Float) -> UIFont {
        return UIFont(name: kRegularFontName, size: kRegularFontSize)!
    }
    
    func defaultNavBarTintColor() -> UIColor {
        return UIColor.flatPinkColorDark()
    }
    
    func defaultBackground() -> UIColor {
        return UIColor.blackColor()
    }
    
    func setDefaultAppearanceProxies() {
        setDefaultNavigationBarAppearance()
    }
    
    private func setDefaultNavigationBarAppearance() {
        var navigationBarAppearance = UINavigationBar.appearance()
        var barButtonAppearance = UIBarButtonItem.appearance()
        var tabBarAppearance = UITabBar.appearance()
        var tabBarItemAppearance = UITabBarItem.appearance()
        
        navigationBarAppearance.barTintColor = defaultNavBarTintColor()
        navigationBarAppearance.tintColor =  UIColor.whiteColor()
        navigationBarAppearance.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: self.defaultNavBarFont!]
        
        barButtonAppearance.setTitleTextAttributes([NSFontAttributeName: self.defaultNavButtonBarFont!], forState: UIControlState.Normal)
        
        tabBarAppearance.barTintColor = UIColor.flatPinkColorDark()
        tabBarAppearance.tintColor = UIColor.whiteColor()
        tabBarItemAppearance.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.lightGrayColor()], forState: .Normal)
        tabBarItemAppearance.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Selected)
    }
}


