//
//  CRMHomeViewController.swift
//  Creams
//
//  Created by Sulley on 13/05/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit

class CRMHomeViewController: UIViewController {
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup the Appearance Manager
        let appearanceManager = CRMAppearanceManager.sharedInstance
        self.view.backgroundColor = appearanceManager.defaultBackground()
        
    }

}
