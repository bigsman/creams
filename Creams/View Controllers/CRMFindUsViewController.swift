//
//  CRMFindUsViewController.swift
//  Creams
//
//  Created by Sulley on 14/05/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit

class CRMFindUsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var stores : [CRMStore] = []
    
    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        readStoresJSON()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = "Find Us"
    }
    
    // MARK: Private Methods
    func readStoresJSON() {
        if let
            path     = NSBundle.mainBundle().pathForResource("stores", ofType: "json"),
            url      = NSURL(fileURLWithPath: path),
            data     = NSData(contentsOfURL: url),
            json     = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: nil) as? NSDictionary,
            items    = json["storesArray"] as? [NSDictionary]
        {
            for store in items {
                var storeName = store["storeName"] as? String
                var address = store["address"] as? String
                var town = store["town"] as? String
                var postcode = store["postcode"] as? String
                var phoneNumber = store["phoneNumber"] as? String
                var emailAddress   = store["emailAddress"] as? String
                
                stores.append(CRMStore(storeName: storeName!, address: address!, town: town!, postcode: postcode!, phoneNumber: phoneNumber!, emailAddress: emailAddress!))
            }
        }
    }
    
    // MARK: UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("StoreCell", forIndexPath: indexPath) as! UITableViewCell
        
        let appearanceManager = CRMAppearanceManager.sharedInstance
        
        cell.textLabel?.text = stores[indexPath.row].town
        cell.textLabel?.font = appearanceManager.defaultFont
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "StoreDetail" {
            if let destination = segue.destinationViewController as? CRMStoreDetailViewController, storeIndex = tableView.indexPathForSelectedRow()?.row {
                if let dc = segue.destinationViewController as? CRMStoreDetailViewController {
                    
                    dc.navigationItem.title = stores[storeIndex].town
                    navigationItem.title = ""
                    dc.store = stores[storeIndex]
                }
            }
        }
    }

}
