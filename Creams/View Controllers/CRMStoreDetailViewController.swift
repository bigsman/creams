//
//  CRMStoreDetailViewController.swift
//  Creams
//
//  Created by Sulley on 15/05/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit
import MapKit

class CRMStoreDetailViewController: UIViewController {
    let regionRadius: CLLocationDistance = 1000

    var store : CRMStore?
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var segmentController: UISegmentedControl!
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    // MARK: View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set initial location in Honolulu
        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        centerMapOnLocation(initialLocation)
        
    }
    
    // MARK: IBActions
    @IBAction func segmentControllerPressed(sender: AnyObject) {
    
    }
}
