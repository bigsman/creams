//
//  AppDelegate.swift
//  Creams
//
//  Created by Sulley on 10/05/2015.
//  Copyright (c) 2015 BigSMan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Setup the Appearance Manager
        let appearanceManager = CRMAppearanceManager.sharedInstance
        appearanceManager.setDefaultAppearanceProxies()

        return true
    }
}

